#include<cstdio>

class Account
{
private:
	//Data member / Field / Property / Attribute
	char name[ 30 ];
	int number;
	float balance;
public:
	//Member Function / Method / operation / behavior / message
	void accept_record( void )
	{
		printf("Name	:	");
		scanf("%s", name);
		printf("Number	:	");
		scanf("%d", &number);
		printf("Balance	:	");
		scanf("%f", &balance);
	}
	void print_Record( void )
	{
		printf("Name	:	%s\n", name);
		printf("Number	:	%d\n", number);
		printf("Balance	:	%f\n", balance);
	}
};

int main( void )
{
	Account acc;	//Instantiation

	acc.Account::accept_record( );	//Message Passing

	acc.Account::print_Record( );	//Message Passing

	return 0;
}
int main2( void )
{
	Account acc;	//Instantiation

	acc.accept_record( );	//Message Passing

	acc.print_Record( );	//Message Passing

	return 0;
}
int main1( void )
{
	Account acc;	//Instantiation
	acc.accept_record( );//acc.accept_record( &acc );
	acc.print_Record( );//acc.print_Record(  &acc );
	return 0;
}
