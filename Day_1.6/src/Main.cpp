#include<stdio.h>
#include<stdlib.h>

//Global Structure
struct Employee
{
	char name[ 30 ];
	int empid;
	float salary;
};
//Global Function
void acceept_record( struct Employee* ptr )
{
	printf("Name	:	");
	scanf("%s",ptr->name);
	printf("Empid	:	");
	scanf("%d",&ptr->empid);
	printf("Salary	:	");
	scanf("%f",&ptr->salary);
}
//Global Function
void print_record( struct Employee* ptr )
{
	printf("Name	:	%s\n",ptr->name);
	printf("Empid	:	%d\n",ptr->empid);
	printf("Salary	:	%f\n",ptr->salary);
}
//Global Function
int main( void )
{
	struct Employee emp;

	acceept_record( &emp );

	print_record( &emp );

	return 0;
}
//Global Function
int main1( void )
{

	struct Employee* ptr = ( struct Employee* )malloc( sizeof( struct Employee ) );

	acceept_record( ptr );

	print_record( ptr );

	free( ptr );
	return 0;
}
