#include<iostream>
using namespace std;

class Complex
{
private:
	int real;
	int imag;
public:
	//Complex *const this;
	void acceptRecord( void )
	{
		cout<<"Real Number	:	";
		cin>>this->real;
		cout<<"Imag Number	:	";
		cin>>this->imag;
	}
	//Complex *const this = &c1
	//Complex other = c2;
	Complex sum( Complex other )	//Complex sum( Complex *const this, Complex other )
	{
		Complex temp;
		temp.real = this->real + other.real;
		temp.imag = this->imag + other.imag;
		return temp;
	}
	//Complex *const this;
	void printRecord( void )
	{
		cout<<"Real Number	:	"<<this->real<<endl;
		cout<<"Imag Number	:	"<<this->imag<<endl;
	}
};
int main( void )
{
	Complex c1, c2, c3;
	c1.acceptRecord( );	//c1.acceptRecord( &c1 );	//10,20
	c2.acceptRecord( );	//c2.acceptRecord( &c2 );	//30,40
	c3 = c1.sum( c2 );	//c3 = c1.sum( &c1, c2 );
	c3.printRecord( );	//c3.printRecord( &c3 );	//40,60
	return 0;
}
