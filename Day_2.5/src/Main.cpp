#include<cstdio>

class Account
{
private:
	char name[ 30 ];
	int number;
	float balance;
public:
	void acceptRecord( void );
	void printRecord( void );
};
//ReturnType ClassName::FuntionName
void Account::acceptRecord( void )
{
	printf("Name	:	");
	scanf("%s", name);
	printf("Number	:	");
	scanf("%d", &number);
	printf("Balance	:	");
	scanf("%f", &balance);
}
void Account::printRecord( void )
{
	printf("Name	:	%s\n", name);
	printf("Number	:	%d\n", number);
	printf("Balance	:	%f\n", balance);
}
int main( void )
{
	Account acc;

	acc.acceptRecord();

	acc.printRecord();
	return 0;
}
